package main

import (
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"strings"
	"strconv"
)

func initParam()map[string]string{
	return map[string]string{
		"apiKey": "xnd_production_OIiIfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV2gg==", //production
		//"apiKey": "xnd_development_P4yAfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV3hg==",
		"url": "https://api.xendit.co/",
	}
}

func main() {
	http.HandleFunc("/disbursment/callback", callback)
	http.ListenAndServe(":7032", nil)
}

func callback(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	//id,_ := jsonparser.GetString(body,"user_id")
	//externalId,_ := jsonparser.GetString(body,"external_id")
	//amount,_ := jsonparser.GetInt(body,"amount")
	//bankCode,_ := jsonparser.GetString(body,"bank_code")
	//accountName,_ := jsonparser.GetString(body,"account_holder_name")
	//description,_ := jsonparser.GetString(body,"disbursement_description")
	status,_ := jsonparser.GetString(body,"status")
	errorCode := 201
	if strings.Compare(status,"COMPLETED"){
		errorCode = 201
	}else{
		errorCode = 404
	}

	resp := `{"error_code":"`+strconv.Itoa(errorCode)+`","message":"`+status+`"}`
	w.WriteHeader(errorCode)
	w.Write([]byte(resp))
}
